import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

train = pd.read_csv(r'./Data Files/train.csv')
#test = pd.read_csv(r'./Data Files/test.csv')

plots = []

fig,axs=plt.subplots(2,2, figsize=(30,13))

axs[0,0].hist(train['Age'],20)
axs[0,0].set_title('Age')
axs[0,0].set_ylabel('Count')

axs[0,1].hist(train['SibSp'],20)
axs[0,1].set_title('SibSp')
axs[0,1].set_ylabel('Count')

axs[1,1].hist(train['Parch'],20)
axs[1,1].set_title('Parch')
axs[1,1].set_ylabel('Count')

axs[1,0].hist(train['Fare'],20)
axs[1,0].set_title('Fare')
axs[1,0].set_ylabel('Count')

fig.savefig('Plots/plot1.jpg')

fig1,ax1=plt.subplots(2,2, figsize=(10,5))

div1=['yes','no']
val1=[train['Survived'].value_counts()[1],train['Survived'].value_counts()[0]]
ax1[0,0].bar(div1,val1)
ax1[0,0].set_title('Survived')

div2=['1st','2nd','3rd']
val2=[train['Pclass'].value_counts()[1],train['Pclass'].value_counts()[2],train['Pclass'].value_counts()[3]]
ax1[0,1].bar(div2,val2)
ax1[0,1].set_title('Pclass')

div3=['Male','Female']
val3=[train['Sex'].value_counts()['male'],train['Sex'].value_counts()['female']]
ax1[1,0].bar(div3,val3)
ax1[1,0].set_xlabel('Sex')

div4=['C','Q','S']
val4=[train['Embarked'].value_counts()['C'],train['Embarked'].value_counts()['Q'],train['Embarked'].value_counts()['S']]
ax1[1,1].bar(div4,val4)
ax1[1,1].set_xlabel('Embarked')

fig1.savefig('Plots/plot2.jpg')




    