

def encode(X):
    from sklearn.preprocessing import LabelEncoder
    label_encoder = LabelEncoder() 

    X.Sex = label_encoder.fit_transform(X.Sex)
    X.Embarked = label_encoder.fit_transform(X.Embarked)