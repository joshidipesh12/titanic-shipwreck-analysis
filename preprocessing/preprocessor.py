


if __name__ == "__main__":
    
    import sys
    sys.path.append('..')

    import numpy as np 
    import pandas as pd
    import matplotlib.pyplot as plt
    import seaborn as sns
    import warnings
    warnings.filterwarnings('ignore')
    %matplotlib inline


    train_df=pd.read_csv("../input/train.csv")
    test_df=pd.read_csv("../input/test.csv")

